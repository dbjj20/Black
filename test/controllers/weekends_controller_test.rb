require 'test_helper'

class WeekendsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @weekend = weekends(:one)
  end

  test "should get index" do
    get weekends_url
    assert_response :success
  end

  test "should get new" do
    get new_weekend_url
    assert_response :success
  end

  test "should create weekend" do
    assert_difference('Weekend.count') do
      post weekends_url, params: { weekend: { week: @weekend.week } }
    end

    assert_redirected_to weekend_url(Weekend.last)
  end

  test "should show weekend" do
    get weekend_url(@weekend)
    assert_response :success
  end

  test "should get edit" do
    get edit_weekend_url(@weekend)
    assert_response :success
  end

  test "should update weekend" do
    patch weekend_url(@weekend), params: { weekend: { week: @weekend.week } }
    assert_redirected_to weekend_url(@weekend)
  end

  test "should destroy weekend" do
    assert_difference('Weekend.count', -1) do
      delete weekend_url(@weekend)
    end

    assert_redirected_to weekends_url
  end
end
