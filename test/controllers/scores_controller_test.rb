require 'test_helper'

class ScoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @score = scores(:one)
  end

  test "should get index" do
    get scores_url
    assert_response :success
  end

  test "should get new" do
    get new_score_url
    assert_response :success
  end

  test "should create score" do
    assert_difference('Score.count') do
      post scores_url, params: { score: { course_id: @score.course_id, score_final: @score.score_final, score_four: @score.score_four, score_one: @score.score_one, score_test: @score.score_test, score_three: @score.score_three, score_two: @score.score_two, student_id: @score.student_id } }
    end

    assert_redirected_to score_url(Score.last)
  end

  test "should show score" do
    get score_url(@score)
    assert_response :success
  end

  test "should get edit" do
    get edit_score_url(@score)
    assert_response :success
  end

  test "should update score" do
    patch score_url(@score), params: { score: { course_id: @score.course_id, score_final: @score.score_final, score_four: @score.score_four, score_one: @score.score_one, score_test: @score.score_test, score_three: @score.score_three, score_two: @score.score_two, student_id: @score.student_id } }
    assert_redirected_to score_url(@score)
  end

  test "should destroy score" do
    assert_difference('Score.count', -1) do
      delete score_url(@score)
    end

    assert_redirected_to scores_url
  end
end
