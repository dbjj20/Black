Rails.application.routes.draw do
  resources :weekends
  resources :semesters
  resources :courses
  resources :grades
  resources :periods
  #---------------------------

#################################### RUTAS DE ASISTENCIAS
  resources :assistances
  get 'teacher/dashboard/editar/asistencias/:id' => 'assistances#teacher_assistances_edit_form', as: 'teacher_editar_asistencia'
  #----------------------------------

##################################### RUTA ROOT
  get 'welcome/index'
  root 'welcome#index'
  #-----------------------
###################################### RUTA RAILS_ADMIN
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  #---------------------------

############################################ RUTA DE LOS PROFESORES
  devise_for :teachers, path_prefix:'profesor'
  resources :teachers
  get 'teacher/curso/:id' => 'teachers#curso', as: 'teacher_curso'
  get 'teacher/dashboard' => 'teachers#dashboard', as: 'teacher_dashboard'
  get 'teacher/dashboard/asistencia/:id' => 'teachers#asistencia_curso', as: 'teacher_asistencia'
  #---------------------------------

############################################ RUTAS DE LOS ESTUDIANTES
  devise_for :students, path_prefix:'estudiante'
  resources :students
  get 'student/dashboard' => 'students#dashboard', as: 'student_dashboard'
  get 'student/courses_curso/:id' => 'students#courses_curso', as: 'student_courses_curso'
  get 'student/grade/:id' => 'students#grade', as: 'student_grade'
  get 'student/profile' => 'students#profile', as: 'student_profile'
  get 'student/change_grade/:id' => 'students#change_grade_student', as: 'change_grade_student'
  post 'student/enroll_student' => 'students#enroll_student', as: 'enroll_student'
  #----------------------------

############################################# RUTA DEL USUARIO ADMIN
  devise_for :users, path_prefix:'usuario'
  resources :users
  #---------------------------

############################################# RUTAS DE LAS NOTAS
  resources :scores
  get 'score/teacher_edit_form/:id' => 'scores#teacher_edit_form', as: 'teacher_edit_form'
  #--------------------
##################################################
  devise_for :parents, path_prefix:'familia'
  resources :parents

# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
