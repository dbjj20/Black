class CreateAssistances < ActiveRecord::Migration[5.0]
  def change
    create_table :assistances do |t|
      t.string :day1
      t.string :day2
      t.string :day3
      t.string :day4
      t.string :day5
      t.string :day6
      t.string :day7
      t.string :today
      t.integer :student_id
      t.integer :weekend_id
      t.integer :semester_id
      t.timestamps
    end
  end
end
