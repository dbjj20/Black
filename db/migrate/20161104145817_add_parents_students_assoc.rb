class AddParentsStudentsAssoc < ActiveRecord::Migration[5.0]
  def up
    create_table :parents_students, id: false do |t|
      t.belongs_to :student, index: true
      t.belongs_to :parent, index: true
    end
  end

  def down
    drop_table :parents_students
  end
end
