class AddTeachersToAssistances < ActiveRecord::Migration[5.0]
  def change
    add_column :assistances, :teacher_id, :integer
  end
end
