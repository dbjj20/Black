class AddAssistancesToTeachers < ActiveRecord::Migration[5.0]
  def change
    add_column :teachers, :assistance_id, :integer
  end
end
