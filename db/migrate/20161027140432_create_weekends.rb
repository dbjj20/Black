class CreateWeekends < ActiveRecord::Migration[5.0]
  def change
    create_table :weekends do |t|
      t.string :week

      t.timestamps
    end
  end
end
