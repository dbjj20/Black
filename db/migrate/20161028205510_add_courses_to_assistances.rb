class AddCoursesToAssistances < ActiveRecord::Migration[5.0]
  def change
    add_column :assistances, :course_id, :integer
  end
end
