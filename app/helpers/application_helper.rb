module ApplicationHelper

	def set_periodo
		Period.all
	end
	def set_grades
		Grade.where(current: true)

	end
	def set_teacher
		Teacher.all
	end

	def set_students
		Student.all
	end

	def set_courses
		Course.all
	end
	def set_users
		  User.all
	end
	def set_weekend
			Weekend.all
	end
	def set_semester
		Semester.all
	end
	def set_parents
		Parent.all
	end
end
