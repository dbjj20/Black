json.extract! assistance, :id, :day, :today, :course_id, :teacher_id, :student_id, :created_at, :updated_at
json.url assistance_url(assistance, format: :json)