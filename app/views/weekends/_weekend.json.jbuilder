json.extract! weekend, :id, :week, :created_at, :updated_at
json.url weekend_url(weekend, format: :json)