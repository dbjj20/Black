json.extract! grade, :id, :name, :period_id, :current, :created_at, :updated_at
json.url grade_url(grade, format: :json)