json.extract! semester, :id, :assistance_id, :user_id, :created_at, :updated_at
json.url semester_url(semester, format: :json)