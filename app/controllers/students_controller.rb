class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy, :change_grade_student, :enroll_student]

  before_action :authenticate_user!, except: [:dashboard, :courses_curso, :grade, :profile]
  before_action :authenticate_student!, only: [:dashboard, :courses_curso, :grade, :profile]

  layout "Admin"
  # GET /students
  # GET /students.json
  def index
    @students = Student.all
  end

  # GET /students/1
  # GET /students/1.json
  def show
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  #----------------------------//////////
  def change_grade_student
  end

  def enroll_student
    grade = Grade.find(params[:id])

    grade.courses.each do |v|
      v.scores.create(student_id: @student.id)
    end

    if @student.grades << grade
      redirect_to students_path, notice: "El estudiante ha sido asignado a este grado"
    else
      logger.alert 'CAMBIO NO SATISFACTIRO'
      render :change_grade_student
    end


  end
# area administrativa de estudiandes-------------------
  def dashboard
    #debe mostrar datos personales del estudiante
    #las materias que esta cursando y el grado actual
    @courses = current_student.grades.actual.first.courses unless current_student.grades.actual.blank?


    #mostrar lo que ya ha cursado
    @grades_old = current_student.grades.where(current: false) unless current_student.grades.where(current: false).blank?

  end
  def courses_curso
    #mostras los datos del curso solicitado, incluyendo las notas
    @course = Course.find(params[:id])
    @scores = @course.scores.find_by student_id: current_student
  end
  def grade
    @grade = Grade.find(params[:id])
    @ss = 1
    @grade.courses.each do |c|
      @ss = @ss + c.scores.where(student_id: current_student).first.score_final if c.scores.any?
    end
    @result = @ss/@grade.courses.count
  end
  def profile
    @student = current_student
  end

#------------------------/-/--/-/-/-/-/-/-/-//-
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit([:name,:last_name, :email, :password])
    end
end
