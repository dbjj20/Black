class Semester < ApplicationRecord
  has_many :assistances
  belongs_to :course
  belongs_to :user
  belongs_to :period
  has_many :students
  validates :name, presence: true

end
