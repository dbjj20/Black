class Assistance < ApplicationRecord

  belongs_to :student
  belongs_to :course
  belongs_to :semester
  belongs_to :weekend
  belongs_to :teacher
end
