class Weekend < ApplicationRecord
    has_many :assistances
    validates :week, presence: true
end
